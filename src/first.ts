const express = require('express');
//import {Database} from "./database"
const dbConnection = require("./database");
const emp = require('./employees')
const app = express();
const env = require('dotenv').config();
const port = process.env.PORT;
const host = process.env.HOST;

//it will used to convert postman json object into json at o/p
app.use(express.json());

console.log("port after reading  .env ==>"+ port)

/*app.get('/employees', (req:any, res:any) => {
    res.send('<h1>Welcome to Home Page of Docker, adding some changes to docker....</h1>');
})
*/

// app.post("/employees", (req:any, res:any) => {

//     console.log(req.body);
//     const user = new emp(req.body);

//     user.save().then(()=>{
//         res.status(201).send(user);
//     }).catch((err:any)=>{
//         res.status(400).send(err)
//     });

// })

//create employee on database using postmon
app.post("/employees", async(req:any, res:any) => {
try{

    console.log(req.body);
    const user = new emp(req.body);
    const createUser = await user.save();
    res.status(201).send(createUser);

}catch(e){
    console.log(e)
}
})

//get the all data from db 
app.get('/employees', async(req:any, res:any) => {
    try{

        const getEmp = await emp.find();
        res.status(200).send(getEmp);

    }catch(err){
        res.status(400).send(err);
    }

})


//get data of only 1 employee bt id
app.get('/employees/:id', async(req:any, res:any) => {
    try{
         
        const _id = req.params.id;
        const single_empData = await emp.findById(_id);
        
        res.status(200).send(single_empData);

    }catch(e){
        res.status(400).send(e);
    }
})

app.listen(port, () => {
    console.log('port is listening on '+host+": "+port);
})
