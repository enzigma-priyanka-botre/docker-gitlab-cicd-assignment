const mongoose = require('mongoose');
const validator = require('validator');

//create schema:
const empSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        validate(value:any){
            if(!validator.isEmail(value)){
                throw new Error("Email is invalid, Enter valid Email..")
            }
        }
    },

    phone: {
        type: Number,
        required: true,
        minlength: 10,
        maxlength: 13,
    },

    address: {
        type:String
    }
})

//create new collection by creating model
const Employee = new mongoose.model("Employee", empSchema);

//export collection
module.exports = Employee;