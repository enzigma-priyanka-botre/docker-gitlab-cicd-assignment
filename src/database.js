"use strict";
exports.__esModule = true;
exports.Database = void 0;
var mongoose = require('mongoose');
var Database = /** @class */ (function () {
    function Database() {
    }
    Database.db = mongoose.connect("mongodb://localhost:27017/Node_kubernetes_Assign", { useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(function () {
        console.log("Coonection Successful..");
    })["catch"](function (error) {
        console.log("Error is: " + error);
    });
    return Database;
}());
exports.Database = Database;
