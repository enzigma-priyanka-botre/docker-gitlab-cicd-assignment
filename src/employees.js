var mongoose = require('mongoose');
var validator = require('validator');
//create schema:
var empSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        validate: function (value) {
            if (!validator.isEmail(value)) {
                throw new Error("Email is invalid, Enter valid Email..");
            }
        }
    },
    phone: {
        type: Number,
        required: true,
        minlength: 10,
        maxlength: 13
    },
    address: {
        type: String
    }
});
//create new collection by creating model
var Employee = new mongoose.model("Employee", empSchema);
//export collection
module.exports = Employee;
