var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
var express = require('express');
//import {Database} from "./database"
var dbConnection = require("./database");
var emp = require('./employees');
var app = express();
var env = require('dotenv').config();
var port = process.env.PORT;
var host = process.env.HOST;
//it will used to convert postman json object into json at o/p
app.use(express.json());
console.log("port after reading  .env ==>" + port);
/*app.get('/employees', (req:any, res:any) => {
    res.send('<h1>Welcome to Home Page of Docker, adding some changes to docker....</h1>');
})
*/
// app.post("/employees", (req:any, res:any) => {
//     console.log(req.body);
//     const user = new emp(req.body);
//     user.save().then(()=>{
//         res.status(201).send(user);
//     }).catch((err:any)=>{
//         res.status(400).send(err)
//     });
// })
//create employee on database using postmon
app.post("/employees", function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var user, createUser, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                console.log(req.body);
                user = new emp(req.body);
                return [4 /*yield*/, user.save()];
            case 1:
                createUser = _a.sent();
                res.status(201).send(createUser);
                return [3 /*break*/, 3];
            case 2:
                e_1 = _a.sent();
                console.log(e_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
//get the all data from db 
app.get('/employees', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var getEmp, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, emp.find()];
            case 1:
                getEmp = _a.sent();
                res.status(200).send(getEmp);
                return [3 /*break*/, 3];
            case 2:
                err_1 = _a.sent();
                res.status(400).send(err_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
//get data of only 1 employee bt id
app.get('/employees/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var _id, single_empData, e_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                _id = req.params.id;
                return [4 /*yield*/, emp.findById(_id)];
            case 1:
                single_empData = _a.sent();
                res.status(200).send(single_empData);
                return [3 /*break*/, 3];
            case 2:
                e_2 = _a.sent();
                res.status(400).send(e_2);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.listen(port, function () {
    console.log('port is listening on ' + host + ": " + port);
});
